# Project 6: Brevet time calculator service
Alex Marozick
Simple listing service from project 5 stored in MongoDB database.

Designed RESTful service to expose what is stored in MongoDB
Designed two different representations: one in csv and one in json. For the above, JSON should be your default representation for the above three basic APIs.
Added a query parameter to get top "k" open and close times. For examples, see below.
Designed consumer programs (e.g., in jQuery) to use the service that you expose. "website" inside DockerRestAPI is an example of that. It is uses PHP. You're welcome to use either PHP or jQuery to consume your services. NOTE: your consumer program should be in a different container like example in DockerRestAPI.

"""
Alex Marozick

This program handles the frontend logic and routes for the website.

Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import json

from pymongo import MongoClient


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.entrydb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    btime = request.args.get("btime")
    bdate = request.args.get("bdate")
    brevet_dist_km = request.args.get("brevet_dist_km")
    print("brevet time: " + btime)
    print("brevet date: " + bdate)
    print("brevet distance: " + brevet_dist_km)

    brevet_start_time = arrow.get(bdate + " " + btime + "-07:00")
    print(brevet_start_time)

    #def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    open_time = acp_times.open_time(km, brevet_dist_km, brevet_start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, brevet_start_time)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/_submit")
def _submit():
    print("In submit in flask_brevets")
    print()
    result = flsk.request.args.get('TableData',999, type=str)
    db.entrydb.delete_many({ })
    list_dicts = json.loads(str(result))
    for item in list_dicts:
        print(item)
    db.entrydb.insertMany(list_dicts)
    return "Nothing"

@app.route("/_display")
def _display():
    print("In display in flask_brevets.py")
    print()
    #db.tododb.deleteMany({ })
    _items = db.entrydb.find()
    starttime = request.args.get("starttime", 999, type=str)
    print(starttime)
    startdate = request.args.get("startdate", 999, type=str)
    print(startdate)
    brevit_dist = request.args.get("brevet_dist", 999, type=str)
    print(brevet_dist)

    items = [item for item in _items]
    if(len(items) == 0):
        #render error template
        return flask.render_template('error.html')
    else:
        return flask.render_template('display.html', items=items, startdate=startdate, starttime=starttime, brevet_dist=brevet_dist)
#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

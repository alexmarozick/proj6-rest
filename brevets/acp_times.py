"""
Alex Marozick

This program contains two functions which calculate the times for the open
and close intervals for each control point.

Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # invalid input: too large
    if(control_dist_km > 1.2*int(brevet_dist_km)):
        print("ERROR: You selected a control point greater than 20% of the total brevet distance\n")
        return
    # valid input: greater than brevet distance
    elif(control_dist_km <= 1.2*int(brevet_dist_km) and control_dist_km > int(brevet_dist_km)):
        if(int(brevet_dist_km) == 200):
            time = 200/34
        elif(int(brevet_dist_km) == 300):
            time = 200/34 + 100/32
        elif(int(brevet_dist_km) == 400):
            time = 200/34 + 200/32
        elif(int(brevet_dist_km) == 600):
            time = 200/34 + 200/32 + 200/30
        elif(int(brevet_dist_km) == 1000):
            time = 200/34 + 200/32 + 200/30 + 400/28

    #normal inputs
    elif(control_dist_km == 0):
        time = 0
    elif(control_dist_km > 0 and control_dist_km <= 200):
        time = control_dist_km/34
    elif(control_dist_km > 200 and control_dist_km <= 400):
        time = 200/34 + (control_dist_km-200)/32
    elif(control_dist_km > 400 and control_dist_km <= 600):
        time = 200/34 + 200/32 + (control_dist_km-400)/30
    elif(control_dist_km > 600 and control_dist_km <= 1000):
        time = 200/34 + 200/32 + 200/30 + (control_dist_km-600)/28
    elif(control_dist_km > 1000 and control_dist_km <= 1300):  # this is unnnessary becuse brevet distance max is 1000 and if control is here it will get caught above
        time = 200/34 + 200/32 + 200/30 + 400/28 + (control_dist_km-1000)/26

    hours = math.floor(time)
    minutes = round((time - hours)*60)
    return brevet_start_time.shift(hours= hours, minutes= minutes).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #use the min speed
    if(control_dist_km > 1.2*int(brevet_dist_km)): # need to implement this on the
    #front end so that you can load a new html page
        print("ERROR: You selected a control point greater than 20% of the total brevet distance\n")
        return
    # valid input: greater than brevet distance
    elif(control_dist_km <= 1.2*int(brevet_dist_km) and control_dist_km > int(brevet_dist_km)):
        if(int(brevet_dist_km) == 200):
            ctime = 200/15
        elif(int(brevet_dist_km) == 300):
            ctime = 300/15
        elif(int(brevet_dist_km) == 400):
            ctime = 400/15
        elif(int(brevet_dist_km) == 600):
            ctime = 600/15
        elif(int(brevet_dist_km) == 1000):
            ctime = 600/15 + 400/11.428
    # normal inputs
    elif(control_dist_km == 0):
        ctime = 1
    elif(control_dist_km > 0 and control_dist_km <= 600):
        ctime = control_dist_km/15
    elif(control_dist_km > 600 and control_dist_km <= 1000):
        ctime = 600/15 + (control_dist_km-600)/11.428
    elif(control_dist_km > 1000 and control_dist_km <= 1300):
        ctime = 600/15 + 400/11.428 + (control_dist_km-1000)/13.333
    elif(control_dist_km > 1300 and control_dist_km <= 1560):
        ctime = 600/15 + 400/11.428 + 300/13.333

    hours = math.floor(ctime)
    minutes = round((ctime - hours)*60)
    return brevet_start_time.shift(hours= hours, minutes= minutes).isoformat()
